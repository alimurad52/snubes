import { combineReducers } from "redux";
import formReducer from "./formReducer";

//create and assign the form reducer object
const rootReducer = combineReducers({
    form: formReducer
});
export default rootReducer;
