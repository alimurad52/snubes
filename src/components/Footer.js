import React from 'react';
import Award from '../assets/logos/award.png';
import {SocialIcons} from "../config/const";

const Footer = () => {
    return (
        <div className="footer-container">
            <div className="_footer-wrapper">
                <div className="__footer-logo-wrapper">
                    <img src={Award} alt="award" />
                </div>
                <div className="__footer-first-sec">
                    <button aria-label="about_us">About us</button>
                    <button aria-label="partner">Become a partner</button>
                    <button aria-label="faq">FAQ</button>
                </div>
                <div className="__footer-second-sec">
                    <button aria-label="imprint">Imprint</button>
                    <button aria-label="tandc">Terms & Conditions</button>
                    <button aria-label="pnp">Privacy Policy</button>
                </div>
                <div className="__footer-third-sec">
                    <a href="mailto:support@snubes.com">support@snubes.com</a>
                    <a href="tel:+49(0)3055645327">+49 (0) 305 5645327</a>
                    <div className="__icon-wrapper">
                        {SocialIcons}
                    </div>
                </div>
            </div>
            <div className="_reference-container">
                <p>© 2019 Snubes GmbH All Rights Reserved.</p>
            </div>
        </div>
    )
}

export default Footer;