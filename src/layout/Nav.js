import React from 'react';
import {ReactComponent as Logo} from '../assets/logos/snubes-logo.svg';
import {ReactComponent as LogoM} from '../assets/logos/snubes-logo-m.svg';
import {ReactComponent as ChevronDown} from '../assets/icons/chevron_down.svg';
import {ReactComponent as BurgerMenuIcon} from '../assets/icons/burger-menu.svg';
import {NavBarItems} from "../config/const";

const Nav = () => {
    return (
        <div className="navbar-container">
            <Logo className="_desktop-logo" />
            <LogoM className="_mobile-logo" />
            <div className="_items-wrapper">
                {
                    NavBarItems.map((item, i) => {
                        return <div key={i} className="__item-wrapper">
                            <span>{item.label}</span>
                            {item.dropdown && <ChevronDown />}
                        </div>
                    })
                }
                <BurgerMenuIcon className="__burger-menu-icon" />
            </div>
        </div>
    )
};

export default Nav;