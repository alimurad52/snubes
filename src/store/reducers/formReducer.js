import {SET_FORM_DATA} from "../actionTypes";

const INITIAL_STATE = {
    companyName: '',
    name: '',
    email: '',
    phone: ''
};

const formReducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_FORM_DATA:
            //append new values to the data object of the store
            return Object.assign({}, state, {
                companyName: action.payload.companyName,
                name: action.payload.name,
                email: action.payload.email,
                phone: action.payload.phone
            })
        default:
            return state;
    }
}

export default formReducer;