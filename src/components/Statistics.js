import React, {useEffect, useState} from 'react';
import ContactForm from "./ContactForm";
import {StatsItems} from "../config/const";
import { useDispatch } from "react-redux";
import { setFormData } from "../store/actions/formAction";
import ContactFormFallback from "./ContactFormFallback";

const Statistics = () => {

    //form states
    const [phone, setPhoneNumber] = useState('');
    const [countryCode, setCountryCode] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [showFallback, setShowFallback] = useState(false);

    //error states
    const [phoneError, setPhoneError] = useState(false);
    const [companyNameError, setCompanyNameError] = useState(false);
    const [nameError, setNameError] = useState(false);
    const [emailError, setEmailError] = useState(false);

    //use dispatch to execute actions
    const dispatch = useDispatch();

    useEffect(() => {
        if (countryCode.length === 0) {
            fetchUserCountryCode();
        }
    }, [countryCode]);

    const fetchUserCountryCode = () => {
        // get country code using the ipapi API
        fetch('https://ipapi.co/json/')
            .then((response) => response.json())
            .then((data) => {
                setCountryCode(data.country_calling_code);
                setPhoneNumber(data.country_calling_code);
            });
    };

    const onChangePhoneNumber = (e) => {
        let value = e.target.value;
        const reg = /^\+\d+$/;
        // check if the input string is less than the length
        // of country code then set country code as the phonenumber value
        if(value.length < countryCode.length) {
            setPhoneNumber(countryCode)
        } else if(reg.test(value)) {
            setPhoneNumber(value);
            setPhoneError(false);
        }
    };

    const onChangeCompany = (e) => {
        let value = e.target.value;
        const reg = /\d/;
        // check if string length is less than or equal to 80 or is length 0
        // to allow user to empty the field. regex expression to check there are no digits
        if((value.length <= 80 && !reg.test(value)) || value.length === 0) {
            setCompanyNameError(false);
            setCompanyName(value);
        }
    };

    const onChangeName = (e) => {
        let value = e.target.value;
        setNameError(false);
        setName(value);
    };

    const onChangeEmail = (e) => {
        let value = e.target.value;
        setEmailError(false);
        setEmail(value);
    };

    const onSubmitForm = (e) => {
        e.preventDefault();
        //a flag to show red outline on all fields at once rather then setting one by one
        let hasError = false
        let reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if(name.length === 0) {
            setNameError(true);
            hasError = true;
        }
        if(phone.length <= countryCode.length) {
            setPhoneError(true);
            hasError = true;
        }
        if(email.length === 0 || !email.match(reg)) {
            setEmailError(true);
            hasError = true;
        }
        if(companyName.length === 0) {
            setCompanyNameError(true);
            hasError = true;
        }
        if (hasError) return;
        dispatch(setFormData(
            {
                name,
                companyName,
                phone,
                email
            }
        ))
        setShowFallback(true);
    };

    return (
        <div className="statistic-container">
            {
                !showFallback && <div className="_contact-form-wrapper">
                    <ContactForm
                        companyName={companyName}
                        onChangeCompany={onChangeCompany}
                        phone={phone}
                        onChangePhoneNumber={onChangePhoneNumber}
                        name={name}
                        onChangeName={onChangeName}
                        email={email}
                        onChangeEmail={onChangeEmail}
                        onSubmitForm={onSubmitForm}
                        emailError={emailError}
                        phoneError={phoneError}
                        companyNameError={companyNameError}
                        nameError={nameError}
                    />
                </div>
            }
            {
                showFallback && <div className="_contact-form-fallback-wrapper">
                    <ContactFormFallback />
                </div>
            }
            <div className="_stats-wrapper">
                <p className="__stats-description">
                    Welcome to Europe's largest call center database
                </p>
                <div className="__stats-items">
                    {
                        StatsItems.map((item, i) => {
                            return <div key={i}>
                                <h1>{item.heading}</h1>
                                <p>{item.description}</p>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Statistics;