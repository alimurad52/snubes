import React from 'react';
import ReactDOM from 'react-dom';
import './scss/main.scss';
import Home from './layout/Home';
import Nav from "./layout/Nav";
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Provider } from 'react-redux';
import { store, persistor } from "./store/store";

ReactDOM.render(
  <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
          <Nav />
          <Home />
      </PersistGate>
  </Provider>,
  document.getElementById('root')
);
