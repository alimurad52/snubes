import React from 'react';
import Statistics from "../components/Statistics";
import Adverts from "../components/Adverts";
import WhyUs from "../components/WhyUs";
import Testimonials from "../components/Testimonials";
import Footer from "../components/Footer";

const Home = () => {
    return (
        <div>
            <Statistics />
            <Adverts />
            <WhyUs />
            <Testimonials />
            <Footer />
        </div>
    )
}

export default Home;