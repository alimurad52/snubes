import React from 'react';
import {WhyUsItems} from "../config/const";

const WhyUs = () => {
    return (
        <div className="why-us-container">
            <div className="_content-wrapper">
                <h1>Why Snubes?</h1>
                {
                    WhyUsItems.map((item, i) => {
                        return <div key={i}>
                            <h4>{item.heading}</h4>
                            <p>{item.description}</p>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default WhyUs;