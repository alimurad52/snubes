import React from 'react';

const ContactForm = (
    {
        companyName,
        onChangeCompany,
        phone,
        onChangePhoneNumber,
        name,
        onChangeName,
        onChangeEmail,
        email,
        onSubmitForm,
        phoneError,
        companyNameError,
        nameError,
        emailError
    }) => {
    return (
        <div className="contact-form-container">
            <div className="_form-description">
                <h1>Find inbound call centers for your company</h1>
                <p>Use our AI and Big Data driven call center sourcing solution</p>
            </div>
            <form onSubmit={onSubmitForm}>
                <div>
                    <label htmlFor="company">Company</label>
                    <input
                        name="company"
                        id="company"
                        type="text"
                        placeholder="Company"
                        className={companyNameError ? "red-outline" : ""}
                        value={companyName}
                        onChange={onChangeCompany}
                    />
                </div>
                <div>
                    <label htmlFor="name">Name</label>
                    <input
                        name="name"
                        id="name"
                        type="text"
                        placeholder="Name"
                        className={nameError ? "red-outline" : ""}
                        value={name}
                        onChange={onChangeName}
                    />
                </div>
                <div>
                    <label htmlFor="phone">Phone</label>
                    <input
                        name="phone"
                        id="phone"
                        type="tel"
                        className={phoneError ? "red-outline" : ""}
                        value={phone}
                        onChange={onChangePhoneNumber}
                    />
                </div>
                <div>
                    <label htmlFor="email">Email</label>
                    <input
                        name="email"
                        id="email"
                        type="text"
                        className={emailError ? "red-outline" : ""}
                        placeholder="name@mail.com"
                        value={email}
                        onChange={onChangeEmail}
                    />
                </div>
                <div className="button-wrapper">
                    <input
                        aria-label="submit"
                        type="submit"
                        value="Get informed"
                    />
                </div>
            </form>
        </div>
    )
}

export default ContactForm;