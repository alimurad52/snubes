import {SET_FORM_DATA} from "../actionTypes";

export const setFormData = (data) => {
    return (dispatch) => {
        dispatch(setFormStoreData(data));
    }
}

//function that dispatches the action type to pass
//the payload to the reducer
function setFormStoreData(payload) {
    return {
        type: SET_FORM_DATA,
        payload: payload
    }
}