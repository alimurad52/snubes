import React from 'react';
import Carousel, {CarouselItem} from "./Carousel";
import {BrandLogos, CarouselContent} from "../config/const";

const Testimonials = () => {
    return (
        <div className="testimonial-container">
            <Carousel>
                {
                    CarouselContent.map((item, i) => {
                        return <CarouselItem
                            key={i}
                            logo={item.logo}
                            pic={item.pic}
                            testimonial={item.description}
                        />
                    })
                }
            </Carousel>
            <div className="_brand-wrapper">
                {
                    BrandLogos.map((item, i) => {
                        return <div key={i}>
                            <img
                                src={item.logo}
                                alt={item.alt}
                            />
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default Testimonials;