import React from 'react';
import {ReactComponent as ReceiveOffer} from "../assets/images/receive_offer.svg";
import {ReactComponent as SignUp} from "../assets/images/sign_up.svg";
import {ReactComponent as SourceSmarter} from "../assets/logos/source_smarter.svg";
import {ReactComponent as TalixoLogo} from "../assets/logos/talixo-logo.svg";
import {ReactComponent as FBLogo} from "../assets/icons/facebook-f-brands.svg";
import {ReactComponent as TwitterLogo} from "../assets/icons/twitter-brands.svg";
import {ReactComponent as LinkedIn} from "../assets/icons/linkedin-in-brands.svg";
//brands images
import Allianz from "../assets/logos/allianz.png";
import CreditShelf from "../assets/logos/creditshelf.png";
import UserCentrics from "../assets/logos/usercentrics.png";
import LaserHub from "../assets/logos/laserhub.png";
import Talixo from "../assets/logos/talixo-logo.png";
//brands images end

export const NavBarItems = [
    {label: 'About Us', dropdown: false},
    {label: 'How it works', dropdown: false},
    {label: 'Become a Partner', dropdown: false},
    {label: 'EUR', dropdown: true},
    {label: 'EN', dropdown: true},
    {label: 'Sign In', dropdown: false},
    {label: 'Get Access', dropdown: false},
];

export const StatsItems = [
    {heading: "500+", description: 'Tender'},
    {heading: "200+", description: 'Callcenter'},
    {heading: "375.000", description: 'Agents available'},
    {heading: "85%", description: 'Faster sourcing'},
];

export const AdvertItems = [
    {
        logo: SignUp,
        label: 'Sign up for free',
        description: 'Signing up and setting up your project takes less than 5 minutes'
    },
    {
        logo: SourceSmarter,
        label: 'Source smarter',
        description: 'Our data-based AI and our experts will help you find exactly what you are looking for'
    },
    {
        logo: ReceiveOffer,
        label: 'Receive offers',
        description: 'With our insights you always receive high-quality proposals at the best price'
    },
];

export const WhyUsItems = [
    {
        heading: 'Trust Know-How',
        description: 'We have more than 20 years of experience and continuously check call centers for their quality and capabilities. Customers like Allianz trust Snubes industry knowledge.'
    },
    {
        heading: 'Time and cost savings',
        description: 'With Snubes you can easily compare prices, quality and availability, and find more potential suppliers. Reach your goal faster and save resources.'
    },
    {
        heading: 'Satisfaction guaranteed',
        description: 'We offer a validated data-based approach to procurement, rather than awarding contracts only based on the lowest price, an existing relationship or "gut feeling".'
    }
];

export const CarouselContent = [
    {
        logo: <TalixoLogo/>,
        description: '“Finding a suitable outsourcing service provider through Snubes was very easy and our personal consultant helped us every step of the way. After a short time we had good offers on the table. An on-site visit confirmed the positive impression and we have now found a matching partner. Thank you for your great support, which has helped us a lot in the selection process.”',
        pic: {
            name: "Jan Brenneke",
            position: "VP Operations"
        },
    },
    {
        logo: <TalixoLogo/>,
        description: '“Finding a suitable outsourcing service provider through Snubes was very easy and our personal consultant helped us every step of the way. After a short time we had good offers on the table. An on-site visit confirmed the positive impression and we have now found a matching partner. Thank you for your great support, which has helped us a lot in the selection process.”',
        pic: {
            name: "John Doe",
            position: "VP Marketing"
        },
    },
    {
        logo: <TalixoLogo/>,
        description: '“Finding a suitable outsourcing service provider through Snubes was very easy and our personal consultant helped us every step of the way. After a short time we had good offers on the table. An on-site visit confirmed the positive impression and we have now found a matching partner. Thank you for your great support, which has helped us a lot in the selection process.”',
        pic: {
            name: "Jane Doe",
            position: "VP Sales"
        },
    }
];

export const BrandLogos = [
    {alt: 'allianz-logo', logo: Allianz},
    {alt: 'creditshelf-logo', logo: CreditShelf},
    {alt: 'usercentric-logo', logo: UserCentrics},
    {alt: 'laserhub-logo', logo: LaserHub},
    {alt: 'talixo-logo', logo: Talixo}
];

export const SocialIcons = [
    <FBLogo key={1} />, <TwitterLogo key={2} />, <LinkedIn key={3} />
];