import React from 'react';
import {AdvertItems} from "../config/const";

const Adverts = () => {
    return (
        <div className="adverts-container">
            {
                AdvertItems.map((item, i) => {
                    return <div key={i}>
                        <item.logo />
                        <p>{item.label}</p>
                        <p>{item.description}</p>
                    </div>
                })
            }
        </div>
    )
}

export default Adverts;