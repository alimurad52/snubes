import React, { useEffect, useState } from "react";
import {ReactComponent as ArrowLeft} from "../assets/icons/arrow-left.svg";
import {ReactComponent as ArrowRight} from "../assets/icons/arrow-right.svg";
import {ReactComponent as ArrowRightM} from "../assets/icons/arrow-right-m.svg";
import {ReactComponent as ArrowLeftM} from "../assets/icons/arrow-left-m.svg";

export const CarouselItem = ({logo, testimonial, pic}) => {
    return (
        <div className="carousel-item">
            <div className="_item-wrapper">
                {logo}
                <p className="__item-testimonial">{testimonial}</p>
                <p className="__item-pic"><strong>{pic.name}</strong>, {pic.position}</p>
            </div>
        </div>
    );
};

const Carousel = ({ children }) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const [paused, setPaused] = useState(false);

    const updateIndex = (newIndex) => {
        if (newIndex < 0) {
            //when user clicks previous on the first slide and the values goes below zero
            //take the slides count and -1
            newIndex = React.Children.count(children) - 1;
        } else if (newIndex >= React.Children.count(children)) {
            //when user clicks next on the last slide, set the slide active index to 0
            newIndex = 0;
        }
        setActiveIndex(newIndex);
    };

    useEffect(() => {
        //an interval to autoplay slides and move to the next one every 3s
        const interval = setInterval(() => {
            if (!paused) {
                updateIndex(activeIndex + 1);
            }
        }, 3000);

        return () => {
            if (interval) {
                clearInterval(interval);
            }
        };
    });

    return (
        <div className="carousel-wrapper">
            <div className="_arrow-left">
                <button
                    aria-label="prev"
                    onClick={() => { updateIndex(activeIndex - 1)}}
                ><ArrowLeft /></button>
            </div>
            <div
                className="_carousel-container"
                onMouseEnter={() => setPaused(true)}
                onMouseLeave={() => setPaused(false)}
            >
                <div
                    className="__slide-wrapper"
                    style={{ transform: `translateX(-${activeIndex * 100}%)` }}
                >
                    {React.Children.map(children, (child, index) => {
                        return React.cloneElement(child, { width: "100%" });
                    })}
                </div>
            </div>
            <div className="_arrow-right">
                <button
                    aria-label="next"
                    onClick={() => { updateIndex(activeIndex + 1)}}
                >
                    <ArrowRight />
                </button>
            </div>
            <div className="_dots">
                <button
                    aria-label="prev-m"
                    className="__arrow-left-m"
                    onClick={() => {updateIndex(activeIndex - 1)}}
                >
                    <ArrowLeftM />
                </button>
                <div>
                    {
                        React.Children.map(children, (child, index) => {
                            return (
                                <button
                                    aria-label={'dot-'+index}
                                    className={'dot ' + (index === activeIndex ? "active" : "")}
                                    onClick={() => {
                                        updateIndex(index);
                                    }}
                                >
                                </button>
                            );
                        })
                    }
                </div>
                <button
                    aria-label="next-m"
                    className="__arrow-right-m"
                    onClick={() => { updateIndex(activeIndex + 1)}}
                >
                    <ArrowRightM />
                </button>
            </div>
        </div>

    );
};

export default Carousel;