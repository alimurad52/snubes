## Demo

[https://snubes.ali-murad.com](https://snubes.ali-murad.com).

## Project setup

1. `cd` into the project root directory
2. run the command `npm install`
3. run `npm start` to run the project in the browser
4. if running on default port, the project should then be available at `http://localhost:3000/`

## Highlights

1. I used `scss` instead of `css` to show off my knowledge of `SASS`. All styling was done with `scss` and no design library was used.

2. At no point throughtout the project was `absolute` positioning used. I used `grid` and `flex` properties to manage the positioning.

3. Fully functional carousel was integrated completely built with React and JavaScript. No external library was used.

4. Redux store was integrated with `persist` to preserve the state upon refresh of the page.

5. `Regex expressions`  were used for form validation.

6. None of the content is hardcoded. All data is being rendered using `arrays` which are stored in `config/conts.js`. This would come in handy if we were to integrate localization in the future or would be required to add further content.

## Lighthouse

Lighthouse reports were genrated on the `build` files which were served locally.

#### Desktop
![Desktop](./src/assets/lighthouse/Desktop.png)

#### Mobile
![Mobile](./src/assets/lighthouse/Mobile.png)

## Justification
1. Pure JavaScript was used. I didn't use TypeScript, as mentioned during the interview, I am relatively new to TypeScript and I didn't wanted to implement my experimental knowledge for the test.

