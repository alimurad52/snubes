import React from "react";
import {useSelector} from "react-redux";

const ContactFormFallback = () => {

    //retrieve values from the store using useSelector hook
    const formData = useSelector(state => state.form);
    return (
        <div className="contact-form-container">
            <div className="_form-description">
                <h1>Thank you for your request!</h1>
                <p>You’ve taken the first step. Our experts will get in touch with you soon.</p>
            </div>
            <hr />
            <form>
                <div>
                    <label htmlFor="company">Company</label>
                    <p>{formData.companyName}</p>
                </div>
                <div>
                    <label htmlFor="name">Name</label>
                    <p>{formData.name}</p>
                </div>
                <div>
                    <label htmlFor="phone">Phone</label>
                    <p>{formData.phone}</p>
                </div>
                <div>
                    <label htmlFor="email">Email</label>
                    <p>{formData.email}</p>
                </div>
            </form>
        </div>
    )
}

export default ContactFormFallback;